swh-charts
==========

Helm charts for swh packages:
- swh: helm chart for swh services:
  - storage-replayer
  - statsd-exporter
  - loaders: various git loaders
  - graphql: graphql rpc service
